package com.vikorlabs.hu.service;

import com.vikorlabs.hu.model.Simplex;
import com.vikorlabs.hu.repository.HelloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class HelloService {

    @Autowired
    private HelloRepository helloRepository;

    @PostConstruct
    public void init() {
        helloRepository.deleteAll();
        Simplex simplex = new Simplex();
        simplex.setMyHello("Hello There!");
        helloRepository.save(simplex);
    }

    public Simplex getMySimplex() {
        return helloRepository.findAll().get(0);
    }


}
