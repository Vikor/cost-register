package com.vikorlabs.hu.repository;

import com.vikorlabs.hu.model.Simplex;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HelloRepository extends JpaRepository<Simplex, Long> {
}
